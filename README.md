<div style="text-align:center">
<b style="font-size:30px">hqLoweCode</b>
<p>基于Vite4+TypeScript的Vue3组件库开发框架</p>
</div>

**hqLoweCode**是一个 **Vue3** 组件库开发环境框架,采用最新的 `Vite4+TypeScript` 为技术栈,支持 `按需加载`,`单元测试`,`自动打包与发布`等功能,让我们能更专注于业务组件的开发。

## 快速启动

- 安装 pnpm

```
npm install pnpm -g
```
- 克隆

```
https://gitlab.com/BobTalk/lowecode-ui.git
```
- 安装依赖

```
pnpm install
```

- 打包组件库与工具库

```
pnpm run build:uat
```

- 启动测试项目

```
pnpm run dev
```

- 启动静态[文档](https://bobtalk.gitlab.io/lowecode-ui/)站点

```
pnpm run docs:dev
```