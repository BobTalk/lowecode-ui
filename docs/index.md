---
layout: home

title: 快速搭建Vue3组件库的框架
titleTemplate: " "

hero:
  name: hq-lowecode
  text: 一个快速搭建Vue3组件库的框架
  tagline: 让你的组件库开发更简单
  image:
    src: /logo.png
    alt: hq-lowecod
  actions:
    - theme: brand
      text: 开始
      link: /guild/introduce.md
    - theme: alt
      text: 在 Gitlab 上查看
      link: https://gitlab.com/BobTalk/lowecode-ui.git

features:
  - icon: 💡
    title: Vue3组件库
    details: 基于vite+TypeScript开发
    link: /develop/component
---
