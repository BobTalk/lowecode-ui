export default {
  title: 'hq-lowecode',
  base: '/lowecode-ui',
  // outDir:'./dist',
  themeConfig: {
    logo:"/logo.png",
    nav: [{ text: '文档', link: '/guild/introduce' }],
    sidebar: {
      '/': [
        {
          text: '引入',
          items: [
            {
              text: '介绍',
              link: '/guild/introduce'
            },
            {
              text: '快速开始',
              link: '/guild/quickstart'
            }
          ]
        },
        {
          text: '开发',
          items: [
            {
              text: '目录结构',
              link: '/develop/'
            },
            {
              text: '组件开发',
              link: '/develop/component'
            },
            {
              text: '全局组件',
              link: '/develop/global'
            },

            {
              text: '打包发布',
              link: '/develop/build'
            }
          ]
        }
      ]
    },

    socialLinks: [{ icon: 'gitlab', link: 'https://gitlab.com/BobTalk/lowecode-ui.git' }]
  }
};
