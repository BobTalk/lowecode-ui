import { createApp } from 'vue';
import App from './app.vue';
import easyest from '@hq-lowecode/components';
import 'hq-lowecode/style.css';
const app = createApp(App);
app.use(easyest);
app.mount('#app');
