
import { defineConfig } from "vite";
import vueJsx from "@vitejs/plugin-vue-jsx";

export default defineConfig({
  plugins: [
    // 添加JSX插件
    vueJsx(),
  ],
    // 这里变更一下端口
  server: {
    port: 3000
  }
});
