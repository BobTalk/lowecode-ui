#!/usr/bin/env sh

# 忽略错误
# set -e

# 构建
npm run docs:build

# 进入待发布的目录
cd docs/.vitepress/dist

git remote add origin https://gitlab.com/BobTalk/lowecode-ui.git
git add -A
git commit -m 'deploy'

git push -f origin gh-pages

cd -
 
rm -rf docs/.vitepress/public 
