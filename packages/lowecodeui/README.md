# 修改package.json中的version

1.0.0 -> 1.0.1

`npm version patch`

1.0.1 -> 1.1.0

`npm version minor`

1.1.0 -> 2.0.0

`npm version major`
