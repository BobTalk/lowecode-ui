import _Icon from './icon.vue';
import { withInstall } from '../../utils';
export const HIcon = withInstall(_Icon);
export default HIcon;
