import { describe, expect, it } from 'vitest';

import { mount } from '@vue/test-utils';
import icon from '../icon.vue';
describe('test icon', () => {
  it('should render slot', () => {
    const wrapper = mount(icon, {
      slots: {
        default: 'easyest'
      }
    });

    expect(wrapper.text()).toContain('Icon1');
  });
  it('should have class', () => {
    const wrapper = mount(icon, {});
    expect(wrapper.classes()).toContain('h-icon');
  });
});
