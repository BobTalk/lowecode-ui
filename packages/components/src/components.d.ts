import * as components from "./index";
declare module "@vue/runtime-core" {
  export interface GlobalComponents {
    HButton: typeof components.Button;
    HIcon: typeof components.Icon;
  }
}
export {};
