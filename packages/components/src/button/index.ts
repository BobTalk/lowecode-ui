import _Button from './button.vue';
import { withInstall } from '../../utils';
export const HButton = withInstall(_Button);
export default HButton;
