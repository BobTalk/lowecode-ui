import delPath from '../utils/delpath';
import { series, parallel, src, dest } from 'gulp';
import { pkgPath, componentPath } from '../utils/paths';
import less from 'gulp-less';
import autoprefixer from 'gulp-autoprefixer';
import run from '../utils/run';
//删除lowecodeui
export const removeDist = () => {
  return delPath(`${pkgPath}/lowecodeui`);
};
// 样式打包公共函数
const buildStyleCommons = (fileUrl, outputUrl) => {
  return src(`${componentPath}/${fileUrl}`)
    .pipe(less())
    .pipe(autoprefixer())
    .pipe(dest(`${pkgPath}/lowecodeui/lib/${outputUrl}`))
    .pipe(dest(`${pkgPath}/lowecodeui/es/${outputUrl}`));
};
//打包样式
export const buildStyle = () => {
  return buildStyleCommons(`src/**/style/**.less`, 'src');
};
// 打包样式变量
export const buildStyleVar = () => {
  return buildStyleCommons(`theme/**/*.less`, 'theme');
};
//打包组件
export const buildComponent = async () => {
  run('pnpm run build', componentPath);
};
export default series(
  async () => removeDist(),
  parallel(
    async () => buildStyle(),
    async () => buildStyleVar(),
    async () => buildComponent()
  )
);
